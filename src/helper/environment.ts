import { getEnvironmentString } from '@kominal/lib-node-environment';

export const MASK_CONTROLLER_URL = getEnvironmentString('MASK_CONTROLLER_URL', 'http://mask2_production_controller:3000');
export const MASK_LISTENER_ADDRESS = getEnvironmentString('MASK_LISTENER_ADDRESS', '127.0.0.1');
