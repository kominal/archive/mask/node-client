import { info } from '@kominal/observer-node-client';
import Axios from 'axios';
import { createServer, Server, Socket } from 'net';
import { MASK_CONTROLLER_URL, MASK_LISTENER_ADDRESS } from './helper/environment';

export async function startProxy(mask: {
	group: string;
	identifier: string;
	family: number;
}): Promise<{ server: Server; ip: string; port: number }> {
	info(`Starting proxy connection...`);
	const { data } = await Axios.get(`${MASK_CONTROLLER_URL}/mask/${mask.group}/${mask.identifier}/${mask.family}`);

	const server = createServer((clientSocket) => {
		const dataSocket = new Socket();
		dataSocket.connect({
			host: data.ip,
			port: data.port,
		});

		clientSocket.on('close', (e) => dataSocket.end());
		dataSocket.on('close', (e) => clientSocket.end());
		clientSocket.on('end', () => dataSocket.end());
		dataSocket.on('end', () => clientSocket.end());
		clientSocket.on('error', (e) => dataSocket.end());
		dataSocket.on('error', (e) => clientSocket.end());

		clientSocket.pause();
		dataSocket.pause();

		const configuration = Buffer.from(JSON.stringify({ ...mask, maskIp: data.maskIp }));

		dataSocket.write(configuration, () => {
			clientSocket.pipe(dataSocket);
			dataSocket.pipe(clientSocket);
			clientSocket.resume();
			dataSocket.resume();
		});
	});

	return new Promise((resolve) =>
		server.listen(0, MASK_LISTENER_ADDRESS, () => {
			info('Mask entrypoint started.');
			const port = Number((server.address() as any).port);
			resolve({ server, ip: MASK_LISTENER_ADDRESS, port });
		})
	);
}
